#ifndef NETWORK_H
#define NETWORK_H

#include "RatingServer.h"

using namespace std;

class CCircularBuffer
{
public:
	char content[2000];
	int dataStart;
	int nextInsert;
	bool restOfSequenceLengthDetermined;
	int restOfSequenceLength;
	bool commandTypeDetermined;
	int commandType;
	
	CCircularBuffer();
	
	void renew();
	int dataLoad();
};

void* get_in_addr(struct sockaddr* sa);
void  copyToBuffer(char* paramBuffer, string paramSendString, size_t paramSendStringLength);
void  bufferToLineArray(CCircularBuffer* circularBuffer, char* target, int length);
void  sendCommand(int paramSocketFd, const void* paramSendBuffer, size_t paramLength);
void  setupConnectionsAndManageCommunications(char* paramListeningPortNr, char* paramMaxConnections);
void  handleIncomingData(int paramSocketFd);

#endif
