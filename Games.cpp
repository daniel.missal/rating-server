// This file contains the game management functions.

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Games.h"

using namespace std;

char gameKey[4] = {0, 0, 0, 0};

CGameListElement* gameListRoot = new CGameListElement(-1, gameKey);;

CGameListElement* gameListStart = gameListRoot;
CGameListElement* gameListEnd = gameListRoot;

CGameListElement::CGameListElement(int paramGameServerSocketFd, char* paramGameKey)		// Trivial constructor
{
	int i;
	
	gameServerSocketFd = paramGameServerSocketFd;
	
	for (i = 0; i < 4; i++)
	{
		gameKey[i] = paramGameKey[i];
	}
}

CGameListElement* addGame(int paramGameServerSocketFd)
{
	int i;
	
	char gameKey[4];
	
	CGameListElement* game;
	
	if ((game = getGameBySocketFd(paramGameServerSocketFd)) != 0)
	{
		removeGame(game);
	}
	
	do
	{
		for (i = 0; i < 4; i++)
		{
			gameKey[i] = (rand() % 256) - 128;
		}
	}
	while (getGameByGameKey(gameKey) != 0);
	
	game = new CGameListElement(paramGameServerSocketFd, gameKey);
	
	gameListEnd->nextElement = game;
	gameListEnd = game;
	
	cout << "Added game (GameServerSocketFd = " << paramGameServerSocketFd
		<< ", gameKey = " << gameKey[0] << gameKey[1] << gameKey[2] << gameKey[3] << ")." << endl << endl;
	
	return game;
}

void removeGame(CGameListElement* paramGameListElement)
{
	CGameListElement* game = gameListStart;
	
	while (game->nextElement != paramGameListElement)
	{
		game = game->nextElement;
	}
	
	if (gameListEnd == paramGameListElement)
	{
		gameListEnd = game;
	}
	
	game->nextElement = game->nextElement->nextElement;
	
	cout << "Removed game (GameServerSocketFd = " << paramGameListElement->gameServerSocketFd << ", gameKey = " 
		<< paramGameListElement->gameKey[0]
		<< paramGameListElement->gameKey[1]
		<< paramGameListElement->gameKey[2]
		<< paramGameListElement->gameKey[3]
		<< ")." << endl << endl;
	
	delete paramGameListElement;
}

CGameListElement* getGameByGameKey(char* paramGameKey)
{
	int i;
	
	CGameListElement* game = gameListStart;
	
	if (gameListStart == gameListEnd)
	{
		return 0;
	}
	
	while (game->nextElement != 0)
	{
		game = game->nextElement;
		
		for (i = 0; i < 4; i++)
		{
			if (game->gameKey[i] != paramGameKey[i])
			{
				continue;
			}
		}
		
		return game;
	}
	
	return 0;
}

CGameListElement* getGameBySocketFd(int paramSocketFd)
{
	int i;
	
	CGameListElement* game = gameListStart;
	
	if (gameListStart == gameListEnd)
	{
		return 0;
	}
	
	while (game->nextElement != 0)
	{
		game = game->nextElement;
		
		if (game->gameServerSocketFd == paramSocketFd)
		{
			return game;
		}
		
	}
	
	return 0;
}
